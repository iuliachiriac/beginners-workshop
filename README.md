# Beginners Python Workshop

## Agenda
1. The Python ecosystem: Python Standard Library + 3rd party packages
1. pip, virtualenv
1. Interactive console + help, dir, type, print
1. Data types: int, float, bool, string, list, dict
1. Control structures: if, for
1. Functions
1. Modules & import statement
1. I/O: Files
1. Modules used in example below:
    * json
    * requests (3rd party)
    * csv
    * datetime.date
    * argparse


## Simple json parsing app
1. Create a function that reads a json from a remote location and returns a Python object.
1. Create a function that writes a list of dictionaries to a csv file (dict keys -> column names) 
1. Create a function for each of the below reports:
    * products ordered by `date_added` descending
    * average price per category
    * number of products per category
    * products with extra info:
        * price + VAT
        * total value
        * total value + VAT
        * number of days since added
1. Using argparse, send the following arguments to your script:
    * `--products_url`: URL that retrieves the products json
    * `--list-reports`: display full list of reports available
    * `--report <report_slug>`: choose a certain report
