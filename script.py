MAX_NR_BICYCLES = 20  # this is a constant (uppercase by convention)
nr_of_bicycles = 22
print(nr_of_bicycles)

multiline_str = '''Somnoroase păsărele
Pe la cuiburi se adună
Se ascund în rămurele
Noapte bună!
'''


# Checking if all bikes are rented
if nr_of_bicycles > MAX_NR_BICYCLES:
    print('No more bicycles available.')
    print('All', MAX_NR_BICYCLES, 'bicycles are rented.')
    print('This is a subblock')
elif nr_of_bicycles > 10:
    print('Hai că merge, dar rămânem cu puține.')
else:
    nr_of_bicycles = nr_of_bicycles + 1
    print('Enjoy the ride!')

print()
print('This is my main program')


person_name = "Matei"
name_random = "Mihai"


def say_hello(name):
    print("Hello,", name, '!')
    print("Today is a beautiful day!")
    print()


say_hello("Ana")
say_hello(person_name)
say_hello(name_random)
return_value = say_hello("Rux")
print("say_hello() returns", return_value)


def get_sum(a, b=0):
    return a + b


return_value = get_sum(10, 20)
print("get_sum() returns", return_value)

return_value = get_sum(10)
print("get_sum() returns", return_value)

return_value = get_sum('10', '20')
print("get_sum() returns", return_value)

