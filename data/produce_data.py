from decimal import Decimal
from factory.faker import faker
import json
import math
import random

CATEGORIES = [
    'electronics',
    'books',
    'entertainment',
    'food',
    'medical',
    'fashion',
]

fake = faker.Faker('ro_RO')
fake.add_provider(faker.providers.date_time)

results = []

for _ in range(1000):
    element = {
        'category': random.choice(CATEGORIES),
        'product': fake.sentence(nb_words=3, variable_nb_words=True, ext_word_list=None),
        'price': float(math.trunc(random.random()*100) / Decimal(100) + random.randint(0, 199)),
        'stock': random.randint(0, 30),
        'date_added': fake.date_time_between(start_date='-5y', end_date='now').strftime('%Y-%m-%d %H:%M:%S'),
    }
    results.append(element)

result = json.dumps(results, indent=4, sort_keys=True)
with open('initial_data.json', 'w') as dst:
    dst.write(result)
